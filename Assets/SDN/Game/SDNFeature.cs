﻿using UnityEngine;
using System.Collections;

public class SDNFeature : MonoBehaviour {
	
	public bool		m_onlyDeveloper;
	public bool		m_onlyAdmin;

	void Start () {
		
		D.Trace("[SDNFeature] Start");
		
		//D.Log("- player is {0}", GameManager.Instance.PlayerGameSettings.PlayerGroup);
		
		//	admin only --
		if (m_onlyAdmin) {
			//if (GameManager.Instance.PlayerGameSettings.PlayerGroup == PlayerGroup.GAME_GROUP_ADMIN) return;		
		}

		//	developer only -- includes admin group
		if (m_onlyDeveloper) {
			//if (GameManager.Instance.PlayerGameSettings.PlayerGroup == PlayerGroup.GAME_GROUP_ADMIN) return;		
			//if (GameManager.Instance.PlayerGameSettings.PlayerGroup == PlayerGroup.GAME_GROUP_DEVELOPER) return;
		}
		
		Destroy(gameObject);
	}
	
}
