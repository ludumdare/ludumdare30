﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

/// <summary>
/// Destroy a GameObject with a Delay.
/// </summary>
/// <remarks>
/// Delay the destruction of a GameObject in your Scene.
/// </remarks>
public class SDNDelayDestroy : MonoBehaviour {
	
	public float			Delay;

	//	MONO
	
	void Start () {
		D.Trace(LogCategory.System, "[SDNDelayDestroy] Start");
		Destroy(gameObject,Delay);
	}
	
}
