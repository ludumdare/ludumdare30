﻿using UnityEngine;
using System.Collections;

public class SDNPlatform : MonoBehaviour {

	public bool			m_windows;
	public bool			m_linux;
	public bool			m_mac;
	public bool			m_ios;
	public bool			m_android;
	public bool			m_web;
	public bool			m_editor;
	
	// Use this for initialization
	void Start () {
	
		if (m_windows && Application.platform == RuntimePlatform.WindowsPlayer) return;
		if (m_linux && Application.platform == RuntimePlatform.LinuxPlayer) return;
		if (m_mac && Application.platform == RuntimePlatform.OSXEditor) return;
		if (m_ios && Application.platform == RuntimePlatform.IPhonePlayer) return;
		if (m_android && Application.platform == RuntimePlatform.Android) return;
		if (m_web && Application.platform == RuntimePlatform.WindowsWebPlayer) return;
		if (m_web && Application.platform == RuntimePlatform.OSXWebPlayer) return;
		if (m_editor && Application.platform == RuntimePlatform.WindowsEditor) return;
		if (m_editor && Application.platform == RuntimePlatform.OSXEditor) return;
		
		DestroyImmediate(gameObject);
	}
	
}
