﻿// Copyright 2013, SpockerDotNet LLC

#if !NETFX_CORE

using System.Security.Cryptography;
using System.IO;
using System.Text;
using System;

public class Crypto
{

    public static string DES_Decrypt(string encryptedString, string key)
    {
        DESCryptoServiceProvider desProvider = new DESCryptoServiceProvider();
        desProvider.Mode = CipherMode.ECB;
        desProvider.Padding = PaddingMode.PKCS7;
        desProvider.Key = Encoding.ASCII.GetBytes(key);
        using (MemoryStream stream = new MemoryStream(Convert.FromBase64String(encryptedString)))
        {
            using (CryptoStream cs = new CryptoStream(stream, desProvider.CreateDecryptor(), CryptoStreamMode.Read))
            {
                using (StreamReader sr = new StreamReader(cs, Encoding.ASCII))
                {
                    return sr.ReadToEnd();
                }
            }
        }
    }

    public static string DES_Encrypt(string decryptedString, string key)
    {
        DESCryptoServiceProvider desProvider = new DESCryptoServiceProvider();
        desProvider.Mode = CipherMode.ECB;
        desProvider.Padding = PaddingMode.PKCS7;
        desProvider.Key = Encoding.ASCII.GetBytes(key);
        using (MemoryStream stream = new MemoryStream())
        {
            using (CryptoStream cs = new CryptoStream(stream, desProvider.CreateEncryptor(), CryptoStreamMode.Write))
            {
                byte[] data = Encoding.Default.GetBytes(decryptedString);
                cs.Write(data, 0, data.Length);
				cs.FlushFinalBlock(); // <-- Add this
				return Convert.ToBase64String(stream.ToArray());
            }
        }
    }
}

#endif

#if NETFX_CORE

using System.IO;
using System.Text;
using System;
using CryptoPCL;

public class Crypto
{
	
	public static string DES_Encrypt(string text, string key)
	{
		return CryptoPCL.Crypto.DES_Encrypt(text, key);
	}
	
	public static string DES_Decrypt(string text, string key)
	{
		return CryptoPCL.Crypto.DES_Decrypt(text, key);
	}
}

#endif

