﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class SDNGame : MonoSingleton<SDNGame> {

	public SDNSettings			GameSettings	{ get; set; }
	public SDNEvents			GameEvents		{ get; set; }

	//	PUBLIC

	//	PRIVATE

	private void __onStart() {

	}

	//	MONO

	void OnEnable() {

	}

	void Start() {

	}
}
