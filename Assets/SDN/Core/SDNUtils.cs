﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SDNUtils  {


	/// <summary>
	/// Load a Json resource and return string
	/// </summary>
	/// <returns>
	/// A string of Json from the file.
	/// </returns>
	/// <param name='filename'>
	/// The name of the file including the Path.
	/// </param>
	/// 
	/// Example:
	/// 
	///    string json = LoadJsonResourceFile("Data/game.txt");
	/// 
	public static string LoadJsonResourceFile(string filename) {
		TextAsset _ta = (TextAsset) Resources.Load(filename, typeof(TextAsset));
		if (_ta != null) {
			return _ta.text;
		} else {
			D.Log(string.Format("TestAsset Resource {0} not found", filename));
			return null;
		}
	}
	
	/// <summary>
	/// Deserializes a Json String and returns a Dictionary
	/// </summary>
	/// <returns>
	/// A Dictionary
	/// </returns>
	/// <param name='json'>
	/// The Json String to Parse.
	/// </param>
	public static Dictionary<string, object> JsonDeserialize(string json) {
		Dictionary<string, object> _d = MiniJSON.Json.Deserialize(json) as Dictionary<string, object>;
		if (_d != null) {
			return _d;
		} else {
			D.Log(string.Format("Unable to Parse Json string {0}", json));	
			return null;
		}
	}
	
	public static void DeleteChildren(Transform parent) {
		foreach(Transform child in parent) {
			UnityEngine.Transform.Destroy(child.gameObject);
		}
	}
	
	public static string DumpProperties(Dictionary<string, object> dict) {
		string _return = "properties:\n";	
		foreach(KeyValuePair<string, object> kvp in dict) {
			_return += string.Format("  key: {0} value: {1}\n", kvp.Key, kvp.Value);
		}
		return _return;
	}

	// Note that Color32 and Color implictly convert to each other. You may pass a Color object to this method without first casting it.
	public static string ColorToHex(Color32 color)
	{
		string hex = color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		return hex;
	}
	 
	public static Color HexToColor(string hex)
	{
		byte r = 0;
		byte g = 0;
		byte b = 0;
		if (hex.Length>=2) r = byte.Parse(hex.Substring(0,2), System.Globalization.NumberStyles.HexNumber);
		if (hex.Length>=4) g = byte.Parse(hex.Substring(2,2), System.Globalization.NumberStyles.HexNumber);
		if (hex.Length>=6) b = byte.Parse(hex.Substring(4,2), System.Globalization.NumberStyles.HexNumber);
		return new Color32(r,g,b, 255);
	}
}
