﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using Holoville.HOTween;

/// <summary>
/// A Class to help Moving GameObject around your Gui.
/// </summary>
public class SDNGuiPanel : MonoBehaviour {

	public bool			AutoHide;
	public Vector2		ShowPosition;
	public Vector2		HidePosition;
	public float		EaseDuration;
	public EaseType		EaseType;

	//	PUBLIC

	public virtual void OnShow() { }
	public virtual void OnHide() { }
	public virtual void OnStart() { }
	
	public virtual void Hide() {
		D.Trace(LogCategory.GUI, "[SDNPanel] Hide");
		__hide();
	}
	
	public void Show() {
		D.Trace("[SDNPanel] Show");
		__show();
	}

	//	PRIVATE

	private Vector3 __newLocalPos(Vector2 vector) {
		D.Trace("[SDNPanel] __newLocalPos");
		D.Detail("- new localPosition is ({0}, {1})", vector.x, vector.y);
		return new Vector3(vector.x, vector.y, transform.localPosition.z);
	}
	
	
	private void __show() {
		
		D.Trace("[SDNPanel] __show");
		__moveTo(__newLocalPos(ShowPosition));
		OnShow();
	}
	
	private void __hide() {
		D.Trace("[SDNPanel] __hide");
		__moveTo(__newLocalPos(HidePosition));
		OnHide();
	}
	
	private void __moveTo(Vector3 pos) {
		D.Trace("[SDNPanel] __moveTo");
		TweenParms tp = new TweenParms();
		tp.Prop("localPosition", pos);
		tp.Ease(EaseType);
		D.Detail("- moving to localPosition ({0}, {1})", pos.x, pos.y);
		HOTween.To(transform, EaseDuration, tp);
	}
	
	//	MONO
	
	void OnEnable() {
		D.Trace("[SDNPanel] OnEnable");
	}

	void Start() {

		D.Trace("[SDNPanel] Start");
		if (AutoHide) transform.localPosition = HidePosition;
		OnStart();
	}
	

}
