﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Resize a Panel to fit the Scene.
/// </summary>
public class SDNGuiPanelFill : MonoBehaviour {

	public Vector2		Margin;
	public bool			FillWidth;
	public bool			FillHeight;

	// Use this for initialization
	void Start () {

		//	get the panel

		tk2dSlicedSprite spr = GetComponent<tk2dSlicedSprite>();
		tk2dCamera camera = Camera.main.GetComponent<tk2dCamera>();

		D.Log(camera.ScreenExtents);

		if (spr == null) {
			D.Warn("Object {0} is missing SliceSprite Component", gameObject.name);
		}

		//	get scene width/height

		float sceneWidth = Screen.width;
		float sceneHeight = Screen.height;

		D.Log("- screen size is {0},{1}", sceneWidth, sceneHeight);

		//	make panel width of the scene and adjust for margins

		float w = spr.dimensions.x;
		float h = spr.dimensions.y;

		if (FillWidth) {
			w = sceneWidth - Margin.x;
		}

		if (FillHeight) {
			h = sceneHeight - Margin.y;
		}

		//	update the panel

		spr.dimensions= new Vector2(w, h);

	}
	
}
