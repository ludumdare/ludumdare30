﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class SDNTweenSwing : MonoBehaviour {

	/// <summary>
	/// How long each Swing lasts in Seconds.
	/// </summary>
	public float		SwingDuration;

	/// <summary>
	/// How much to Swing on each Rotation.
	/// </summary>
	public float 		SwingAmount;
	

	void Start() {
		
		TweenParms parms = new TweenParms();
		parms.Prop("rotation", new Vector3(0,0,transform.rotation.z + SwingAmount), true);
		parms.Loops(-1, LoopType.Yoyo);
		parms.Ease(EaseType.Linear);
		HOTween.To(transform, SwingDuration, parms);
	}
}
