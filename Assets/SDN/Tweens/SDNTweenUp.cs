﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class SDNTweenUp : MonoBehaviour {
	
	public float			m_duration;
	public float			m_delay;
	public float			m_height;
	public bool				m_random = true;

	// Use this for initialization
	void Start () {
	
		float _x = 0;
		if (m_random) _x = Random.Range(-50,50);
		HOTween.To(transform, m_duration, "position", new Vector3(transform.position.x + _x, transform.position.y + m_height, transform.position.z));
	}
	
}
