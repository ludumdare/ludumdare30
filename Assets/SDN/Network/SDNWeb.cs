
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// A Simple Class for Sending HTTP Requests
/// </summary>
/// <remarks>
/// Use this class when you want to send a HTTP request to
/// a web server. Use the <see cref="SendRequestDelegate"/> to
/// delegate a method to listen for the response from the 
/// request.
/// </remarks>
public class SDNWeb : MonoSingleton<SDNWeb> {
	
	public delegate void SendRequestDelegate(WWW www);

	private Queue<SendQueuePackage>		sendQueue;
	private SDNTask						queueTask;
	
	//	PUBLIC

	public WWW CreateRequest(string url, string uri, string data) {
		
		D.Trace(LogCategory.Network, "[SDNWeb] CreateRequest(url, uri, data)");
		return __createRequest(url, uri, data);
	}
	
	public void SendRequest(WWW www, SendRequestDelegate del) {
		D.Trace("[SDNWeb] SendRequest");
		D.Log(LogCategory.Network, "- adding http request to the queue", www.url);
		sendQueue.Enqueue(new SendQueuePackage(www, del));
	}
	
	//	PRIVATE

	private WWW __createRequest(string url, string uri, string data) {
		D.Trace(LogCategory.Network, "[SDNWeb] createRequest");
		WWWForm form = new WWWForm();
		form.AddField("data", data);
		WWW www = new WWW( url + uri, form);
		return www;
	}
	
	private IEnumerator __sendResponse(WWW www, SendRequestDelegate del) {
		D.Trace(LogCategory.Network, "[SDNWeb] sendResponse(www, del)");
		yield return www;
		del(www);
		
	}

	private IEnumerator __workQueue() {

		while(true) {

			D.Detail("- send request queue contains {0} items", sendQueue.Count);
			if (sendQueue.Count > 0) {
				D.Detail("- getting the send request package");
				SendQueuePackage queuePackage = (SendQueuePackage) sendQueue.Dequeue();
				D.Detail("- making the request to {0}", queuePackage.SendQueueRequest.url);
				SDNEvents.Instance.Raise(new SDNWebSendStartEvent());
				yield return queuePackage.SendQueueRequest;
				SDNEvents.Instance.Raise(new SDNWebSendStopEvent());
				queuePackage.SendQueueDelegate(queuePackage.SendQueueRequest);
			}
			yield return new WaitForSeconds(1);
		}
	}

	//	MONO

	void OnEnable() {
		D.Trace(LogCategory.Network, "[SDNWeb] OnEnable");
		sendQueue = new Queue<SendQueuePackage>();
	}

	void Start() {
		D.Trace(LogCategory.Network, "[SDNWeb] Start");
		queueTask = new SDNTask(__workQueue());
	}

	void OnDisable() {
		queueTask.Stop();
	}
	
}

public enum HttpResponses {
	
	HTTP_OK,
	HTTP_ERROR,
	HTTP_BAD_REQUEST
}

public class SendQueuePackage {

	public WWW							SendQueueRequest		{ get; set; }
	public SDNWeb.SendRequestDelegate	SendQueueDelegate		{ get; set; }

	public SendQueuePackage(WWW www, SDNWeb.SendRequestDelegate Delegate) {

		SendQueueRequest = www;
		SendQueueDelegate = Delegate;
	}

}
