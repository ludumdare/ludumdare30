﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SDNScoreoid : MonoSingleton<SDNScoreoid> {

	public string			ScoreoidApiKey			{ get; set; }
	public string			ScoreoidGameId			{ get; set; }
	public string			ScoreoidEncryptionKey	{ get; set; }
	public string			ScoreoidResponse		{ get; set; }
	public string			ScoreoidUrl				{ get; set; }
	public string			ScoreoidPlatform		{ get; set; }

	private Dictionary<string, object>	requestData;

	//	===	PUBLIC
	
	public void GetGame() {

		D.Trace(LogCategory.Network, "[SDNScoreoid] GetGame");
		D.Detail(LogCategory.Network, "- creating http package to get game information");

		requestData.Clear();
		__sendRequest("/getGame", __getGameResponse);	
	}
	

	//	===	PRIVATE

	private void __getGameResponse(WWW www) {

		D.Trace(LogCategory.Network, "[SDNScoreoid] __getGameResponse");
		SDNScoreoidEvent e = __getData(www);
		if (!e.Error) {
			SDNEvents.Instance.Raise(e);
		} else {
			SDNScoeroidErrorEvent w = new SDNScoeroidErrorEvent();
			w.www = www;
			SDNEvents.Instance.Raise(w);
		}
	}

	private void __sendRequest(string Method, SDNWeb.SendRequestDelegate DelegateMethod) {

		WWWForm form = new WWWForm();

		//	if the request needs to be encrypted then convert
		//	the request and send
		if (!string.IsNullOrEmpty(ScoreoidEncryptionKey)) {

			string e = "";
			foreach(KeyValuePair<string, object> kvp in requestData) {
				e += string.Format("{0}={1}&", kvp.Key, kvp.Value);
			}
			e += string.Format("api_key={0}", ScoreoidApiKey);
			D.Detail("- string to encrypt \n{0}", e);
			string s = Crypto.DES_Encrypt(e, ScoreoidEncryptionKey);
			D.Detail("- encrypted string \n{0}", s);
			form.AddField("s", s);
		} else {

			form.AddField("api_key", ScoreoidApiKey);
		}

		form.AddField("game_id", ScoreoidGameId);

		WWW www = new WWW(ScoreoidUrl + Method, form);
		SDNWeb.Instance.SendRequest(www, DelegateMethod);
	}
	
	private SDNScoreoidEvent __getData(WWW www) {
		
		D.Trace(LogCategory.Network, "[SDNScoreoid] __getData");

		SDNScoreoidEvent e = new SDNScoreoidEvent();
		e.Error = false;
		e.Data = "";

		if (string.IsNullOrEmpty(www.error)) {
			D.Detail("- no errors from Scoreoid");
			D.Fine(LogCategory.Network, "- returned data from Scoreoid\n\n{0}\n", www.text);
			if (string.IsNullOrEmpty(ScoreoidEncryptionKey)) {
				D.Fine("- the data from Scoreoid is not encrypted");
				e.Data = www.text;
			} else {
				D.Fine("- the data from Scoreoid is encrypted");
				e.Data = Crypto.DES_Decrypt(www.text, ScoreoidEncryptionKey);
			}
		} else {
			D.Warn("- error trying to get data from Scoreoied was {0}", www.error);
			e.Error = true;	
		}
		
		return e;
		
	}
	
	//	===	MONO
	
	void OnEnable() {

		requestData = new Dictionary<string, object>();
	}
	
}
