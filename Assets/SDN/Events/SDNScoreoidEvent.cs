﻿
//	Copyright 2012-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

/// <summary>
/// A Generic Scoreoid Event
/// </summary>
/// <remarks>
/// Returns the Data of the last Scoreoid event and also the Error
/// code, if an error was encountered.
/// </remarks>
public class SDNScoreoidEvent : SDNEvent {

	public bool					Error 	{ get; set; }
	public object				Data 	{ get; set; }
}
