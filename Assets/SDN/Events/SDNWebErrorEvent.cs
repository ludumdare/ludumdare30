﻿
//	Copyright 2012-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Generic Web Error Event
/// </summary>
/// <remarks>
/// In the event of an Error on any HTTP request, this event
/// will be broadcasted to any listeners.
/// </remarks>
/// 
public class SDNWebErrorEvent : SDNEvent {

	/// <summary>
	/// The Unity HTTP Request Object.
	/// </summary>
	/// <value>The HTTP Request</value>
	/// 
	public WWW			www		{ get; set; }
}
