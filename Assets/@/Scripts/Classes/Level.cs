﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Level {

	public string					LevelId			{ get; set; }	//	the unique id of the level
	public string					LevelName		{ get; set; }	//	the name of the level
	public string					LevelTheme		{ get; set; }	//	(optional) theme
	public Player 					LevelPlayer		{ get; set; }	//	player
	public Dictionary<int, Planet>	LevelPlanets	{ get; set; }	//	planets

	//	PUBLIC

	public Level() {

		LevelId = "none";
		LevelName = "none";
		LevelPlayer = new Player();
		LevelPlanets = new Dictionary<int, Planet>();
	}

	//	PRIVATE


}
