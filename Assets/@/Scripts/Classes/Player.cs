﻿using UnityEngine;
using System.Collections;

public class Player {

	public int		PlayerStartingPlanet		{ get; set; }
	public string	PlayerStartingCargo			{ get; set; }
	public int 		PlayerStartingFuel			{ get; set; }
	public int		PlayerScore					{ get; set; }
	public int		PlayerWater					{ get; set; }
	public int		PlayerEnergy				{ get; set; }
	public int		PlayerFood					{ get; set; }
	public int		PlayerOre					{ get; set; }
	public int		PlayerFuel					{ get; set; }

}
