﻿using UnityEngine;
using System.Collections;

public class Planet {

	public enum PlanetResources {

		PLANET_RESOURCE_NONE,
		PLANET_RESOURCE_WATER,
		PLANET_RESOURCE_ENERGY,
		PLANET_RESOURCE_ORE,
		PLANET_RESOURCE_FOOD
	}

	public int				PlanetId				{ get; set; }	//	id of the planet - used for player starting position
	public int				PlanetSize				{ get; set; }	//	size of the planet
	public PlanetResources	PlanetProducesResource	{ get; set; }	//	what this planet produces
	public int				PlanetProducesAmount	{ get; set; }
	public PlanetResources	PlanetRequiresResource	{ get; set; }	//	what this planet needs
	public int				PlanetRequiresAmount	{ get; set; }
	public Vector2			PlanetPosition			{ get; set; }	//	position in grid

}
