﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSettings {

	public string		GameName			{ get; set; }	//	the name of the game
	public string		GameVersion			{ get; set; }	//	the version
	public string 		GameCopyright		{ get; set; }	//	my copyright
	public string		GameWebsite			{ get; set; }	//	url for game website
	public string		GameOnline			{ get; set; }	//	is the device/computer online
	public string		GameFirstScene		{ get; set; }	//	first scene to run after bootstrap

	public string 		AnalyticsAccount	{ get; set; }	//
	public string		AnalyticsDomain		{ get; set; }	//
	public string		AnalyticsGameName	{ get; set; }	//

	private Dictionary<string, object>	settings;

	//	PUBLIC

	public void Load() {

		D.Trace("[GameSettings] Load");
		__loadResourceFile();
		__loadSettings();
	}

	//	PRIVATE

	private void __loadResourceFile() {

		string data = SDNUtils.LoadJsonResourceFile("game");
		Dictionary<string, object> dict = MiniJSON.Json.Deserialize(data) as Dictionary<string, object>;
		if (dict.ContainsKey("settings")) {
			settings = dict["settings"] as Dictionary<string, object>;
		} else {
			D.Error("could not find settings in game resource file");
		}
	}

	private void __loadSettings() {

		if (settings.ContainsKey("gameName"))			GameName = (string)settings["gameName"];
		if (settings.ContainsKey("gameVersion"))		GameVersion = (string)settings["gameVersion"];
		if (settings.ContainsKey("gameCopyright"))		GameCopyright = (string)settings["gameCopyright"];
		if (settings.ContainsKey("gameWebsite"))		GameWebsite = (string)settings["gameWebsite"];
		if (settings.ContainsKey("gameFirstScene"))		GameFirstScene = (string)settings["gameFirstScene"];

		if (settings.ContainsKey("analyticsAccount"))	AnalyticsAccount = (string)settings["analyticsAccount"];
		if (settings.ContainsKey("analyticsDomain"))	AnalyticsDomain = (string)settings["analyticsDomain"];
		if (settings.ContainsKey("analyticsGameName"))	AnalyticsGameName = (string)settings["analyticsGameName"];
	}

}
