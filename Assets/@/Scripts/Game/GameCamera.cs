﻿using UnityEngine;
using System.Collections;

public class GameCamera : MonoBehaviour {

	public Transform	CameraTarget;
	public float		CameraSmoothingSpeed;
	public float		CameraZoomMax;
	public float		CameraZoomMin;
	public float		CameraZoom;
	public float		CameraZoomSpeed;

	private tk2dCamera	theCamera;
	private float		currentZoom;

	//	PUBLIC

	public void ZoomIn() {

		CameraZoom += (Time.deltaTime * CameraZoomSpeed);
	}

	public void ZoomOut() {

		CameraZoom += (Time.deltaTime * -CameraZoomSpeed);
	}

	//	PRIVATE

	private void __updateCameraPosition() {

		return;
		Vector3 tpos = Vector3.zero;
		if (CameraTarget != null)
			tpos = CameraTarget.position;
		Vector3 newpos = Vector3.Lerp(transform.position, tpos, Time.deltaTime * CameraSmoothingSpeed);
		transform.position = newpos;
	}

	private void __updateCameraZoom() {

		return;
		CameraZoom = Mathf.Clamp(CameraZoom, CameraZoomMin, CameraZoomMax);
		float newzoom = Mathf.Lerp(currentZoom, CameraZoom, Time.deltaTime * CameraZoomSpeed);
		currentZoom = newzoom;
		theCamera.ZoomFactor = currentZoom;
		D.Watch("GameCamera", "currentZoom", currentZoom);
	}


	void OnEnable() {

		D.Trace("[GameCamera] OnEnable");
		D.Detail("registering GameCamera");
		GameManager.Instance.GameCamera = this;
		theCamera = GetComponent<tk2dCamera>();
	}

	void Update() {

		__updateCameraPosition();
		__updateCameraZoom();

	}
}
