﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameMusic : MonoBehaviour {

	public AudioClip		HomeMusic;
	public AudioClip		BackgoundMusic;
	public AudioClip		WinMusic;
	public AudioClip		LoseMusic;

	public void PlayHomeMusic() {
		__playMusic(HomeMusic);
	}

	public void PlayBackgroundMusic() {
		__playMusic(BackgoundMusic);
	}
	
	public void PlayWinMusic() {
		__playMusic(WinMusic);
	}
	
	public void PlayLoseMusic() {
		__playMusic(LoseMusic);
	}
	
	private void __playMusic(AudioClip clip) {
		
		D.Trace("[GameMusic] __playMusic");

		if (audio.clip != clip) {
			audio.Stop();
			audio.clip = clip;
			audio.Play();
		}
	}
	
	void OnEnable() {
		D.Trace("[GameMusic] OnEnable");
		GameManager.Instance.GameMusic = this;
		DontDestroyOnLoad(gameObject);
		//gameObject.AddComponent<AudioSource>();
	}
	
	void Start() {
		
		D.Trace("[GameMusic] Start");
	}
	
}
