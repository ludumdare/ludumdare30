﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager> {

	public GameAnalytics		GameAnalytics		{ get; set; }
	public GameLevelBuilder		GameLevelBuilder	{ get; set; }
	public GameSettings			GameSettings		{ get; set; }
	public GameLevel			GameLevel			{ get; set; }
	public GamePlayer			GamePlayer			{ get; set; }
	public GameCamera			GameCamera			{ get; set; }
	public GameMusic			GameMusic			{ get; set; }
	public GameSounds			GameSounds			{ get; set; }
	public GameScore			GameScore			{ get; set; }
	public int					CurrentLevel		{ get; set; }

	private string				levelName;

	//	PUBLIC

	public void Init() {

		D.Trace("[GameManager] Init");

		GameAnalytics = new GameAnalytics();
		GameLevelBuilder = new GameLevelBuilder();
		GameSettings = new GameSettings();
		GameScore = new GameScore();

		CurrentLevel = 1;
		levelName = "level-" + CurrentLevel.ToString();

		GameSettings.Load();
	}

	public GameObject LoadPrefab(string prefab) {

		D.Trace("[GameManager] LoadPrefab");
		D.Detail("trying to load prefab {0}", prefab);
		GameObject go = (GameObject)Instantiate(Resources.Load("Game/" + prefab));

		if (go == null)
			D.Warn("unable to load prefab {0}", prefab);

		return go;
	}

	public void StartLevel() {
		__startLevel();
	}

	public void StartLevel(GameLevel gameLevel) {

		D.Trace("[GameManager] StartLevel");
		GameLevel = gameLevel;
		__startLevel();

	}

	//	PRIVATE

	private void __startLevel() {

		D.Trace("[GameManager] __startLevel");

		GameManager.Instance.GameAnalytics.Log("play_level", GameLevel.Level.LevelName, 1);
		D.Fine("starting level {0}", GameLevel.Level.LevelName);
		GameLevelBuilder.Build(GameLevel);
		D.Detail("the player is starting at planet {0} with {1} cargo spots", 
		         GameLevel.Level.LevelPlayer.PlayerStartingPlanet, 
		         GameLevel.Level.LevelPlayer.PlayerStartingCargo);
		GameCamera.CameraTarget = GamePlayer.transform;
		GamePlayer.PlayerController.StartLevel();
		GameMusic.PlayBackgroundMusic();

	}

	//	MONO

}
