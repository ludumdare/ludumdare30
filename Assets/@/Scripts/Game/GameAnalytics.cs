﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public class GameAnalytics {
	
	public bool				Active	{ get; set; }
	
	//	PUBLIC
	
	public void Init() {
		
		D.Trace("[GameAnalytics] Init");
		__init();
	}
	
	public void Log(string action, string label, int value) {
	
		D.Trace("[GameAnalytics] Log (action, label, value)");
		Log (GameManager.Instance.GameSettings.AnalyticsGameName, GameManager.Instance.GameSettings.AnalyticsGameName, action, label, value);
	}
	
	public void Log(string page, string category, string action, string label, int value) {
	
		if (!Active) return;

		D.Trace("[GameAnalytics] Log (page, category, action, label, value)");
		GoogleAnalyticsHelper.LogEvent(page, category, action, label, value);
	}
	
	//	PRIVATE
	
	private void __init() {
		
		D.Trace("[GameAnalytics] __init");
		
		GoogleAnalyticsHelper.Settings(
			GameManager.Instance.GameSettings.AnalyticsAccount,
			GameManager.Instance.GameSettings.AnalyticsDomain);
	}
	
}
