﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameLevel {

	public Level						Level		{ get; set; }
	public Dictionary<int, GamePlanet>	Planets		{ get; set; }
	public int							Score;

	public GameLevel() {

		D.Trace("[GameLevel]");
		Planets = new Dictionary<int, GamePlanet>();
	}

}
