using UnityEngine;
using System.Collections;

public class GameLevelBuilder {

	//	PUBLIC

	public void Build(GameLevel gameLevel) {

		D.Trace("[GameLevelBuilder] Build");
		__buildLevel(gameLevel);

	}

	//	PRIVATE

	private void __buildLevel(GameLevel gameLevel) {

		D.Trace("[GameLevelBuilder] __buildLevel");

		D.Detail("building level {0}", gameLevel.Level.LevelName);
		D.Detail("there are {0} planets to add to the scene", gameLevel.Level.LevelPlanets.Count);

		__addPlanets(gameLevel);
		__addPlayer(gameLevel);
	}

	private void __addPlanets(GameLevel gameLevel) {

		D.Trace("[GameLevelBuilder] __addPlanets");
		D.Log("adding planets to scene");

		GameObject root = GameObject.Find("@Planets");

		foreach(Planet p in gameLevel.Level.LevelPlanets.Values) {

			D.Trace("adding planet {0}", p.PlanetId);

			GameObject go;

			if (p.PlanetProducesAmount == 0) {
				go = GameManager.Instance.LoadPrefab("GameMoon");
				go.name = "GameMoon";

			} else {

				go = GameManager.Instance.LoadPrefab("GamePlanet");
				go.name = "GamePlanet";
			}


			go.transform.position = p.PlanetPosition * 8;
			go.transform.parent = root.transform;
			GamePlanet planet = go.GetComponent<GamePlanet>();
			planet.Planet = p;
			gameLevel.Planets.Add(p.PlanetId, planet);
			//Vector3 size = Vector3.one;
			//if (p.PlanetSize <= 0) p.PlanetSize = 1;
			//size *= 1 + p.PlanetSize/4;
			//planet.GetComponent<tk2dSprite>().scale = size;
			
			switch (p.PlanetProducesResource) {
			case Planet.PlanetResources.PLANET_RESOURCE_WATER:
				planet.GetComponent<tk2dSprite>().color = Color.blue;
				break;
			case Planet.PlanetResources.PLANET_RESOURCE_ENERGY:
				planet.GetComponent<tk2dSprite>().color = Color.red;
				break;
			case Planet.PlanetResources.PLANET_RESOURCE_FOOD:
				planet.GetComponent<tk2dSprite>().color = Color.green;
				break;
			case Planet.PlanetResources.PLANET_RESOURCE_ORE:
				planet.GetComponent<tk2dSprite>().color = Color.yellow;
				break;
			}

		}
	}

	private void __addPlayer(GameLevel level) {

		D.Trace("[GameLevelBuilder] __addPlayer");
		D.Log("adding player to scene");

		GameObject go = GameManager.Instance.LoadPrefab("GamePlayer");
		go.name = "GamePlayer";
		go.transform.position = level.Planets[0].transform.position;
		go.transform.position = new Vector3(go.transform.position.x + 4.5f, go.transform.position.y + 4.5f, 0);
		GameManager.Instance.GamePlayer = go.GetComponent<GamePlayer>();
	}

}
