﻿using UnityEngine;
using System.Collections;

public class GamePlanet : MonoBehaviour {

	public Planet		Planet;
	public tk2dSprite	ProducesSprite;
	public tk2dSprite	RequiresSprite;
	public tk2dTextMesh	ProducesText;
	public tk2dTextMesh	RequiresText;

	public 

	//	PUBLIC

	//	PRIVATE

	//	MONO

	void Update() {

		if (ProducesText == null) return;

		ProducesText.text = Planet.PlanetProducesAmount.ToString();
		RequiresText.text = Planet.PlanetRequiresAmount.ToString();

		switch (Planet.PlanetProducesResource) {
		case Planet.PlanetResources.PLANET_RESOURCE_WATER:
			ProducesSprite.SetSprite("ResourceWater");
			break;
		case Planet.PlanetResources.PLANET_RESOURCE_ENERGY:
			ProducesSprite.SetSprite("ResourceEnergy");
			break;
		case Planet.PlanetResources.PLANET_RESOURCE_ORE:
			ProducesSprite.SetSprite("ResourceOre");
			break;
		case Planet.PlanetResources.PLANET_RESOURCE_FOOD:
			ProducesSprite.SetSprite("ResourceFood");
			break;
		}

		switch (Planet.PlanetRequiresResource) {
		case Planet.PlanetResources.PLANET_RESOURCE_WATER:
			RequiresSprite.SetSprite("ResourceWater");
			break;
		case Planet.PlanetResources.PLANET_RESOURCE_ENERGY:
			RequiresSprite.SetSprite("ResourceEnergy");
			break;
		case Planet.PlanetResources.PLANET_RESOURCE_ORE:
			RequiresSprite.SetSprite("ResourceOre");
			break;
		case Planet.PlanetResources.PLANET_RESOURCE_FOOD:
			RequiresSprite.SetSprite("ResourceFood");
			break;
		}
	}
}
