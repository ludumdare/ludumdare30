﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameLevelManager : MonoSingleton<GameLevelManager> {

	public GameLevel Load(string level) {
		
		D.Trace("[GameLevelManager] Load");
		return __loadLevel(level);	
	}
	

	//	PRIVATE
	
	private GameLevel __loadLevel(string level) {
		
		D.Trace("[GameLevelManager] __loadLevel");

		GameLevel _return = new GameLevel();
		Level gl = new Level();

		//	get the level from resources
		string _data = SDNUtils.LoadJsonResourceFile(level);
		Dictionary<string, object> data = MiniJSON.Json.Deserialize(_data) as Dictionary<string, object>;

		//	get the level information
		if (data.ContainsKey("level")) {
			Dictionary<string, object> l = data["level"] as Dictionary<string, object>;

			gl.LevelId = (string)l["levelId"];
			gl.LevelName = (string)l["levelName"];

			//	get the player information
			if (l.ContainsKey("player")) {
				Dictionary<string, object> p = l["player"] as Dictionary<string, object>;
				gl.LevelPlayer.PlayerStartingPlanet = (int)(long)p["playerStartingPlanet"];
				gl.LevelPlayer.PlayerStartingCargo = (string)p["playerStartingCargo"];
				gl.LevelPlayer.PlayerStartingFuel = (int)(long)p["playerStartingFuel"];
			}
			
			//	get the planets
			if (l.ContainsKey("planets")) {
				List<object> o = l["planets"] as List<object>;
				foreach(Dictionary<string, object> _p in o) {
					Planet pl = new Planet();
					pl.PlanetId = (int)(long)_p["planetId"];

					string planetProduces = (string)_p["planetProduces"];

					switch (planetProduces.Substring(1,1)) {
						case "N":
							pl.PlanetProducesResource = Planet.PlanetResources.PLANET_RESOURCE_NONE;
							break;
						case "W":
							pl.PlanetProducesResource = Planet.PlanetResources.PLANET_RESOURCE_WATER;
							break;
						case "E":
							pl.PlanetProducesResource = Planet.PlanetResources.PLANET_RESOURCE_ENERGY;
							break;
						case "O":
							pl.PlanetProducesResource = Planet.PlanetResources.PLANET_RESOURCE_ORE;
							break;
						case "F":
							pl.PlanetProducesResource = Planet.PlanetResources.PLANET_RESOURCE_FOOD;
							break;
					}

					pl.PlanetProducesAmount = int.Parse(planetProduces.Substring(0,1));

					string planetRequires = (string)_p["planetRequires"];

					switch (planetRequires.Substring(1,1)) {
					case "N":
						pl.PlanetRequiresResource = Planet.PlanetResources.PLANET_RESOURCE_NONE;
						break;
					case "W":
						pl.PlanetRequiresResource = Planet.PlanetResources.PLANET_RESOURCE_WATER;
						break;
					case "E":
						pl.PlanetRequiresResource = Planet.PlanetResources.PLANET_RESOURCE_ENERGY;
						break;
					case "O":
						pl.PlanetRequiresResource = Planet.PlanetResources.PLANET_RESOURCE_ORE;
						break;
					case "F":
						pl.PlanetRequiresResource = Planet.PlanetResources.PLANET_RESOURCE_FOOD;
						break;
					}

					pl.PlanetRequiresAmount = int.Parse(planetRequires.Substring(0,1));

					int x = (int)(long)_p["planetPositionX"];
					int y = (int)(long)_p["planetPositionY"];
					pl.PlanetSize = (int)(long)_p["planetSize"];

					pl.PlanetPosition = new Vector2(x,y);
					gl.LevelPlanets.Add(pl.PlanetId, pl);
				}
			}

		}

		_return.Level = gl;
		return _return;
	}

	//	MONO
}
