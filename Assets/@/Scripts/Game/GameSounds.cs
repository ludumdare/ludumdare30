﻿
//	Copyright 2013-2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameSounds : MonoBehaviour {
	
	public AudioClip		ShipEngine;
	public AudioClip		ShipCrash;
	public AudioClip		ShipLost;
	public AudioClip		ShipPickup;
	public AudioClip		ShipLaunch;
	public AudioClip		GameFanfare;
	public AudioClip		GameBeep;
	public AudioClip		GameBonus;

	private GameObject		goShipEngine;

	//	PUBLIC

	public void PlayShipEngine() {
		goShipEngine = __playSoundLoop(ShipEngine);
	}

	public void StopShipEngine() {
		Destroy(goShipEngine);
	}

	public void PlayShipLaunch() {
		__playSound(ShipLaunch);
	}
	
	public void PlayShipLost() {
		__playSound(ShipLost);
	}
	
	public void PlayShipPickup() {
		__playSound(ShipPickup);
	}
	
	public void PlayShipCrash() {
		__playSound(ShipCrash);
	}
	
	//	PRIVATE
	
	private void __playSound(AudioClip clip) {
		
		D.Trace("[GameSounds] __playSound");
		__playSound(clip, 1.0f, 0.5f);
	}

	private void __playSound(AudioClip clip, float pitch, float volume) {

		D.Trace("[GameSounds] __playSound");
		//audio.pitch = pitch;
		//audio.PlayOneShot(clip, volume);
		//	reset
		//audio.pitch = 1.0f;

		GameObject go = new GameObject("Sound (Temp)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.volume = volume;
		gs.pitch = pitch;
		gs.Play();
		Destroy(go, clip.length + 0.1f);
	}

	private GameObject __playSoundLoop(AudioClip clip) {

		D.Trace("[GameSounds] __playSound");
		//audio.pitch = pitch;
		//audio.PlayOneShot(clip, volume);
		//	reset
		//audio.pitch = 1.0f;
		
		GameObject go = new GameObject("Sound (Temp)");
		AudioSource gs = go.AddComponent<AudioSource>();
		gs.clip = clip;
		gs.loop = true;
		gs.Play();
		return go;
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[GameSounds] OnEnable");
		GameManager.Instance.GameSounds = this;	
	}
	
}
