﻿using UnityEngine;
using System.Collections;

public class GameStars : MonoBehaviour {

	public GameObject		GameStarPrefab;

	void OnEnable() {

		//	big stars
		for(int i = 0; i < 5; i++) {
			Vector3 pos = new Vector3(Random.Range(0,90), Random.Range(0, 70),1);
			GameObject go = (GameObject)Instantiate(GameStarPrefab, pos, Quaternion.identity);
			go.transform.parent = transform;
		}
		for(int i = 0; i < 10; i++) {
			Vector3 pos = new Vector3(Random.Range(0,90), Random.Range(0, 70),1);
			GameObject go = (GameObject)Instantiate(GameStarPrefab, pos, Quaternion.identity);
			go.transform.parent = transform;
			go.transform.localScale = new Vector3(0.5f, 0.5f, 1);
		}
		for(int i = 0; i < 25; i++) {
			Vector3 pos = new Vector3(Random.Range(0,90), Random.Range(0, 70),1);
			GameObject go = (GameObject)Instantiate(GameStarPrefab, pos, Quaternion.identity);
			go.transform.parent = transform;
			go.transform.localScale = new Vector3(0.25f, 0.25f, 1);
		}
		for(int i = 0; i < 50; i++) {
			Vector3 pos = new Vector3(Random.Range(0,90), Random.Range(0, 70),1);
			GameObject go = (GameObject)Instantiate(GameStarPrefab, pos, Quaternion.identity);
			go.transform.parent = transform;
			go.transform.localScale = new Vector3(0.1f, 0.1f, 1);
		}
	}
}
