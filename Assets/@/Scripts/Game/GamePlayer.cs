﻿using UnityEngine;
using System.Collections;

public class GamePlayer : MonoBehaviour {

	public PlayerController		PlayerController;

	//	PUBLIC

	//	PRIVATE

	//	MONO

	void OnEnable() {

		D.Trace("[GamePlayer] OnEnable");
		D.Detail("registering GamePlayer");
		GameManager.Instance.GamePlayer = this;
		PlayerController = transform.GetComponent<PlayerController>();
	}

	void Start() {

	}

}
