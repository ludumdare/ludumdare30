﻿
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public static class BootstrapEnd {

	public static void Start() {
		
		D.Trace(LogCategory.System, "[BootstrapEnd] Start");

		//	goto the first scene
		Application.LoadLevel(GameManager.Instance.GameSettings.GameFirstScene);
	}
}
