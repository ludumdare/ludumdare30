﻿
//	Copyright 2014 SpockerDotNet LLC

using UnityEngine;
using System.Collections;

public static class BootstrapStart {

	public static void Start() {
		
		Debug.LogWarning("[BootstrapStart] Start");	

		Holoville.HOTween.HOTween.Init(true, false, true);

		GameManager.Instance.Init();


		//	init logger
		
		D.Log(GameManager.Instance.GameSettings.GameName);
		D.Log(GameManager.Instance.GameSettings.GameCopyright);
		
		//	analytics - start game (unless we are in the editor)

		GameManager.Instance.GameAnalytics.Active = true;

#if UNITY_EDITOR
		//GameManager.Instance.GameAnalytics.Active = false;
#endif

		GameManager.Instance.GameAnalytics.Log("game", "start", 1);
	}

}
