﻿using UnityEngine;
using System.Collections;

public static class BootstrapPlayer {

	public static void Start () {
		
		D.Trace(LogCategory.System, "[BootstrapPlayer] Start");
		PlayerSettings.Instance.Load();
		
	}
}
