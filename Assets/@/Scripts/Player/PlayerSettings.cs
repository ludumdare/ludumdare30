﻿using UnityEngine;
using System.Collections;

public class PlayerSettings : MonoSingleton<PlayerSettings> {


	//	PUBLIC

	public void Load() {

		D.Trace("[PlayerSettings] Load");
		__loadSettings();
	}

	public void Save() {

		D.Trace("[PlayerSettings] Save");
		__saveSettings();
	}

	//	PRIVATE

	private void __loadSettings() {

		D.Trace("[PlayerSettings] __loadSettings");
		
	}

	private void __saveSettings() {

		D.Trace("[PlayerSettings] __saveSettings");
		
	}

	//	MONO
}
