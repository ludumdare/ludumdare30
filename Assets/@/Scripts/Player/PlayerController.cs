﻿using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class PlayerController : MonoBehaviour {

	public float		PlayerOrbitSpeed;		//	default orbit speed
	public float		PlayerOrbitSpeedMax;	//	the max that we can go
	public float		PlayerLauchDecay;		//	decay of orbit after aborted launch
	public float		PlayerTravelDecay;		//	decay the travelling speed
	public float		PlayerOrbitDistance;	//	default distance from center of planet
	public GamePlanet	PlayerPlanet;			//	current planet we are orbiting

	public bool			inOrbit;				//	are we in orbit?

	private float		currentSpeed;			//	current orbit speed (set by PlayerOrbitSpeed on launch)
	private bool		isLaunching;			//	are we trying to launch?
	private bool		isTravelling;			//	are we travelling somewhere?
	private bool 		isMovingToPlanet;		//	is the ship moving to the planet?
	private bool		isEnteringOrbit;		//	ship is entering orbit

	private Player		player;
	private bool		active;

	//	PUBLIC

	public void StartLevel() {

		D.Trace("[PlayerController] StartLevel");
		__startLevel();

	}

	//	PRIVATE

	private void __startLevel() {

		D.Trace("[PlayerController] __startLevel");
		GameManager.Instance.GamePlayer.PlayerController.PlayerPlanet = GameManager.Instance.GameLevel.Planets[GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerStartingPlanet];
		player = GameManager.Instance.GameLevel.Level.LevelPlayer;

		//	figure out starting resources

		string res = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerStartingCargo.Substring(1,1);
		int amt = int.Parse(GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerStartingCargo.Substring(0,1));

		if (res == "W")
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater = amt;
		if (res == "E")
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy = amt;
		if (res == "O")
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre = amt;
		if (res == "F")
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood = amt;

		player.PlayerFuel = player.PlayerStartingFuel;
		GameManager.Instance.GameSounds.PlayShipEngine();
		active = true;
	}

	private void __input() {

		bool hitPlanet = false;
		RaycastHit2D hit;

		if (Input.GetMouseButton(0)) {

			Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			Debug.DrawRay(pos, -Vector2.up);
			hit = Physics2D.Raycast(pos, -Vector2.up);

			if (hit.collider != null) {
				D.Watch("PlayerController", "hit", hit.collider.name);
				hitPlanet = true;
			}

		}


		//	if we are not launching and we touched a planet
		if (!isLaunching & hitPlanet) {
			
			isLaunching = true;
		}
		
		if (isLaunching) {

			//	when the mouse has been lifted
			if (Input.GetMouseButtonUp(0)) {

				isLaunching = false;
				inOrbit = false;
				isTravelling = true;
				GameManager.Instance.GameSounds.PlayShipLaunch();
				player.PlayerFuel -= 1;
				//GameManager.Instance.GameCamera.CameraTarget = GameManager.Instance.GamePlayer.transform;
			} else {
				//GameManager.Instance.GameCamera.ZoomIn();
			}
		}

		//	if we were launching and we are no longer touching the planet
		if (isLaunching & !hitPlanet) {
			
			isLaunching = false;
		}
		
	}

	private void __updateOrbit() {

		transform.RotateAround (PlayerPlanet.transform.position, Vector3.forward, currentSpeed * Time.deltaTime);

		if (!isLaunching) {
			currentSpeed = Mathf.Lerp(currentSpeed, PlayerOrbitSpeed, PlayerLauchDecay * Time.deltaTime);
			GameManager.Instance.GameCamera.ZoomOut();
		}
	}

	private void __updateLaunching() {

		currentSpeed += 1;
		Mathf.Clamp(currentSpeed, PlayerOrbitSpeed, PlayerOrbitSpeedMax);
	}

	private void __updateTravelling() {

		//	move the player forward
		transform.Translate(-Vector2.right * (currentSpeed/4) * Time.deltaTime);

		//	decay the speed
		currentSpeed = Mathf.Lerp(currentSpeed, 0, PlayerTravelDecay * Time.deltaTime);
		GameManager.Instance.GameCamera.ZoomIn();

		//	if we are almost stopped and we have not hit a planet then find
		//	the nearest one and go there

		if (currentSpeed < PlayerOrbitSpeed) {

			float dist = -1;

			//	find all planets
			GameObject[] planets = GameObject.FindGameObjectsWithTag("Planet");

			foreach(GameObject go in planets) {

				float d = Vector2.Distance(transform.position, go.transform.position);
				if (dist < 0 || d < dist) {
					PlayerPlanet = go.GetComponent<GamePlanet>();
					dist = d;
				}
			}

			isTravelling = false;
			isMovingToPlanet = true;
			currentSpeed = PlayerOrbitSpeed * 4;
			GameManager.Instance.GameCamera.CameraZoom = GameManager.Instance.GameCamera.CameraZoomMin;
		}
	}

	private void __updateMovingToPlanet() {

		//	get the distance

		float dist = Vector2.Distance(transform.position, PlayerPlanet.transform.position);
		D.Watch("PlayerController", "distance", dist);

		if (dist <= PlayerOrbitDistance + 1) {
			isMovingToPlanet = false;
			isEnteringOrbit = true;
		}

		//	move the player towards planet
		var dir = PlayerPlanet.transform.position - transform.position;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		Quaternion to = Quaternion.AngleAxis(angle - 180, Vector3.forward);
		transform.rotation = Quaternion.Slerp(transform.rotation, to, Time.deltaTime * 3.0f);

		transform.Translate(-Vector2.right * (PlayerOrbitSpeed) * Time.deltaTime);
	}

	private void __updateEnteringOrbit() {

		float dist = Vector2.Distance(transform.position, PlayerPlanet.transform.position);
		D.Watch("PlayerController", "distance", dist);
		//GameManager.Instance.GameCamera.CameraZoom = GameManager.Instance.GameCamera.CameraZoomMin;

		GameManager.Instance.GameSounds.PlayShipCrash();

		isEnteringOrbit = false;

		//transform.Translate(-Vector2.right * (currentSpeed/4) * Time.deltaTime);

		TweenParms tp1 = new TweenParms();
		tp1.Prop("eulerAngles", new Vector3(0,0,359f));
		tp1.Ease(EaseType.Linear);
		tp1.OnComplete(__afterRoll);
		HOTween.To(transform, 0.75f, tp1);
	}

	private void __afterRoll() {

		var dir = PlayerPlanet.transform.position - transform.position;
		var angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
		Quaternion to = Quaternion.AngleAxis(angle +90, Vector3.forward);
		//transform.rotation = Quaternion.Slerp(transform.rotation, to, Time.deltaTime * 1.0f);

		TweenParms tp1 = new TweenParms();
		tp1.Prop("rotation", to);
		tp1.OnComplete(__afterRotate);
		HOTween.To(transform, 0.25f, tp1);
	}

	private void __afterRotate() {

		GameManager.Instance.GameCamera.CameraTarget = null;
 		inOrbit = true;
	}
	
	//	MONO
	
	void OnEnable() {
		
		D.Trace("[PlayerController] OnEnable");
		currentSpeed = PlayerOrbitSpeed;
		isLaunching = false;
		isTravelling = false;
		isEnteringOrbit = false;
		isMovingToPlanet = true;
	}

	void Start() {

	}

	void Update() {


		//D.ClearWatch("PlayerController");
		D.Watch("PlayerController", "inOrbit", inOrbit);
		D.Watch("PlayerController", "isLaunching", isLaunching);
		D.Watch("PlayerController", "isTravelling", isTravelling);
		D.Watch("PlayerController", "isMovingToPlanet", isMovingToPlanet);
		D.Watch("PlayerController", "isEnteringOrbit", isEnteringOrbit);
		D.Watch("PlayerController", "currentSpeed", currentSpeed);

		//	are we in orbit?

		if (inOrbit) __updateOrbit();
		if (isLaunching) __updateLaunching();
		if (isTravelling) __updateTravelling();
		if (isMovingToPlanet) __updateMovingToPlanet();
		if (isEnteringOrbit) __updateEnteringOrbit();

		if (!active) return;
		if (inOrbit) __input();

	}

	void OnTriggerEnter2D(Collider2D other) {
		
		D.Detail("player collided with {0}", other.name);

		if (!isLaunching && !inOrbit && other.tag == "Planet") {

			PlayerPlanet = other.gameObject.transform.GetComponent<GamePlanet>();
			inOrbit = false;
			isLaunching = false;
			isTravelling = false;
			isMovingToPlanet = true;
			isEnteringOrbit = false;
			currentSpeed = PlayerOrbitSpeed;

			if (PlayerPlanet.Planet.PlanetProducesResource == Planet.PlanetResources.PLANET_RESOURCE_WATER) {
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_ENERGY && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_FOOD && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_ORE && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
			}

			if (PlayerPlanet.Planet.PlanetProducesResource == Planet.PlanetResources.PLANET_RESOURCE_ENERGY) {
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_WATER && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_FOOD && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_ORE && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
			}

			if (PlayerPlanet.Planet.PlanetProducesResource == Planet.PlanetResources.PLANET_RESOURCE_ORE) {
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_WATER && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_ENERGY && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_FOOD && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
			}
			
			if (PlayerPlanet.Planet.PlanetProducesResource == Planet.PlanetResources.PLANET_RESOURCE_FOOD) {
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_WATER && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_ENERGY && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
				if (PlayerPlanet.Planet.PlanetRequiresResource == Planet.PlanetResources.PLANET_RESOURCE_ORE && 
				    GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre > 0) {
					int take = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre;
					int give = PlayerPlanet.Planet.PlanetProducesAmount;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre += -take;
					PlayerPlanet.Planet.PlanetProducesAmount += -give;
					GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood += give;
					PlayerPlanet.Planet.PlanetRequiresAmount += -take;
					GameManager.Instance.GameSounds.PlayShipPickup();
				}
			}
			
		}

		//	check win condition
		bool win = true;
		GameObject[] planets = GameObject.FindGameObjectsWithTag("Planet");
		
		foreach(GameObject go in planets) {
			GamePlanet p = go.GetComponent<GamePlanet>();
			if (p.Planet.PlanetRequiresAmount > 0) win = false;
		}

		if (win) {
			GameObject.Find("@PanelWin").GetComponent<SDNGuiPanel>().Show();
			active = false;
			GameManager.Instance.GameMusic.PlayWinMusic();
		} else {

			if (player.PlayerFuel <= 0) {
				GameObject.Find("@PanelFail").GetComponent<SDNGuiPanel>().Show();
				active = false;
			}
		}

	}
	
	void OnDrawGizmos() {

		if (PlayerPlanet == null) return;

		Gizmos.color = Color.yellow;

		//	show selected planet

		Gizmos.DrawWireSphere(PlayerPlanet.transform.position, 10.0f);

		//	draw line from ship to planet

		Gizmos.DrawLine(transform.position, PlayerPlanet.transform.position);
	}
	
}
