﻿using UnityEngine;
using System.Collections;

public class Bootstrap : MonoBehaviour {

	void Start () {

		D.Trace("[Bootstrap] Start");
	
		BootstrapStart.Start();
		BootstrapGame.Start();
		BootstrapPlayer.Start();
		BootstrapCloud.Start();
		BootstrapGraphics.Start();
		BootstrapEnd.Start();
	}
	
}
