﻿using UnityEngine;
using System.Collections;

public class Game : MonoBehaviour {

	public string		LevelName;

	//	PUBLIC

	//	PRIVATE

	//	MONO

	void Start() {

		if (GameManager.Instance.GameLevel == null) {
			//GameManager.Instance.Init();
			GameManager.Instance.StartLevel(GameLevelManager.Instance.Load(LevelName));
		} else {
			GameManager.Instance.StartLevel();
		}
	}

}
