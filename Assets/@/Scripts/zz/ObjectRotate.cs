﻿using UnityEngine;
using System.Collections;

public class ObjectRotate : MonoBehaviour {

	public float PlanetRotateSpeed = -25.0f;
	public float OrbitSpeed = 10.0f;
	
	void Update() {
		// planet to spin on it's own axis
		//transform.Rotate(transform.up * PlanetRotateSpeed * Time.deltaTime);
		
		// planet to travel along a path that rotates around the sun
		transform.RotateAround (Vector3.zero, Vector3.forward, OrbitSpeed * Time.deltaTime);
	}

}
