using UnityEngine;
using System.Collections;
using Holoville.HOTween;

public class RotateSprite : MonoBehaviour {

	public float		Speed;

	// Use this for initialization
	void Start () {
	
	TweenParms parms = new TweenParms();
	
	//parms.Prop("position", new Vector3(10,20,30)); // Position tween
	parms.Prop("rotation", new Vector3(0,0,360), true); // Rotation tween
	//parms.Prop("localScale", new Vector3(1.5f,1.5f,1.5f)); // Scale tween
	parms.Ease(EaseType.Linear); // Easing type
	//parms.Delay(1); // Initial delay
	parms.Loops(-1);
		
		
	HOTween.To(gameObject.transform, Speed, parms );	}

}
