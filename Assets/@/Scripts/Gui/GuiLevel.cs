﻿using UnityEngine;
using System.Collections;

public class GuiLevel : MonoBehaviour {

	public void Level01() {
		__playLevel("1");
	}

	public void Level02() {
		__playLevel("2");
	}
	
	public void Level03() {
		__playLevel("3");
	}
	
	public void Level04() {
		__playLevel("4");
	}
	
	public void Level05() {
		__playLevel("5");
	}
	
	public void Level06() {
		__playLevel("6");
	}
	
	public void Level07() {
		__playLevel("7");
	}
	
	public void Level08() {
		__playLevel("8");
	}
	
	public void Level09() {
		__playLevel("9");
	}
	
	public void Level10() {
		__playLevel("10");
	}

	public void Back_Click() {
		Application.LoadLevel("Home");
	}

	private void __playLevel(string level) {
		GameManager.Instance.CurrentLevel = int.Parse(level);
		string l = "level-" + level;
		GameManager.Instance.GameLevel = GameLevelManager.Instance.Load(l);
		Application.LoadLevel("Game");

	}
}
