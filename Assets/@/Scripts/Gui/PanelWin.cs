﻿using UnityEngine;
using System.Collections;

public class PanelWin : MonoBehaviour {


	public void BackButton_Click() {
		Application.LoadLevel("Levels");
	}

	public void NextButton_Click() {

		GameManager.Instance.CurrentLevel += 1;
		string level = "level-" + GameManager.Instance.CurrentLevel.ToString();
		GameManager.Instance.GameLevel = GameLevelManager.Instance.Load(level);
		Application.LoadLevel("Game");
	}

	public void RetryButton_Click() {

		string level = "level-" + GameManager.Instance.CurrentLevel.ToString();
		GameManager.Instance.GameLevel = GameLevelManager.Instance.Load(level);
		Application.LoadLevel("Game");
	}
}
