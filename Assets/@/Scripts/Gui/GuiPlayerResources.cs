﻿using UnityEngine;
using System.Collections;

public class GuiPlayerResources : MonoBehaviour {

	public tk2dTextMesh		WaterText;
	public tk2dTextMesh		EnergyText;
	public tk2dTextMesh		OreText;
	public tk2dTextMesh		FoodText;

	// Update is called once per frame
	void Update () {

		if (GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater < 0)
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater = 0;

		if (GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy < 0)
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy = 0;
		
		if (GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre < 0)
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre = 0;
		
		if (GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood < 0)
			GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood = 0;
		
		WaterText.text = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerWater.ToString();
		EnergyText.text = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerEnergy.ToString();
		OreText.text = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerOre.ToString();
		FoodText.text = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFood.ToString();
	}
}
