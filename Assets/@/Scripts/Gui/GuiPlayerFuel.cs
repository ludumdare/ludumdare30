﻿using UnityEngine;
using System.Collections;

public class GuiPlayerFuel : MonoBehaviour {

	public tk2dTextMesh		FuelText;

	// Update is called once per frame
	void Update () {
		FuelText.text = GameManager.Instance.GameLevel.Level.LevelPlayer.PlayerFuel.ToString();
	}
}
