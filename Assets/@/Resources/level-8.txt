{
	"level":
	{
		"levelId": "test",
		"levelName": "1-08"
		"player":
		{
			"playerStartingPlanet":	0,
			"playerStartingCargo": "3O",
			"playerStartingFuel": 5
		},
		"planets":
		[
			{
				"planetId": 1,
				"planetSize": 1,
				"planetPositionX": 9,
				"planetPositionY": 2,
				"planetProduces": "3W",
				"planetRequires": "3O"
			},			
			{
				"planetId": 2,
				"planetSize": 0,
				"planetPositionX": 5,
				"planetPositionY": 1,
				"planetProduces": "5E",
				"planetRequires": "3W"
			},
			{
				"planetId": 3,
				"planetSize": 0,
				"planetPositionX": 2,
				"planetPositionY": 3,
				"planetProduces": "3O",
				"planetRequires": "3E"
			},
			{
				"planetId": 4,
				"planetSize": 0,
				"planetPositionX": 5,
				"planetPositionY": 4,
				"planetProduces": "3W",
				"planetRequires": "3O"
			},
			{
				"planetId": 5,
				"planetSize": 0,
				"planetPositionX": 3,
				"planetPositionY": 6,
				"planetProduces": "3F",
				"planetRequires": "3W"
			},
			{
				"planetId": 0,
				"planetSize": 0,
				"planetPositionX": 10,
				"planetPositionY": 5,
				"planetProduces": "0N",
				"planetRequires": "0N"
			},
		]
	}
}
