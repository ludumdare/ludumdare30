{
	"level":
	{
		"levelId": "test",
		"levelName": "1-05"
		"player":
		{
			"playerStartingPlanet":	0,
			"playerStartingCargo": "2O",
			"playerStartingFuel": 6
		},
		"planets":
		[
			{
				"planetId": 1,
				"planetSize": 2,
				"planetPositionX": 8,
				"planetPositionY": 2,
				"planetProduces": "3W",
				"planetRequires": "2O"
			},			
			{
				"planetId": 2,
				"planetSize": 4,
				"planetPositionX": 4,
				"planetPositionY": 3,
				"planetProduces": "2F",
				"planetRequires": "3W"
			},
			{
				"planetId": 3,
				"planetSize": 1,
				"planetPositionX": 2,
				"planetPositionY": 1,
				"planetProduces": "3O",
				"planetRequires": "2F"
			},
			{
				"planetId": 4,
				"planetSize": 1,
				"planetPositionX": 5,
				"planetPositionY": 6,
				"planetProduces": "2E",
				"planetRequires": "3O"
			},
			{
				"planetId": 5,
				"planetSize": 3,
				"planetPositionX": 2,
				"planetPositionY": 5,
				"planetProduces": "3W",
				"planetRequires": "2E"
			},			
			{
				"planetId": 0,
				"planetSize": 1,
				"planetPositionX": 9,
				"planetPositionY": 5,
				"planetProduces": "0N",
				"planetRequires": "0N"
			},
		]
	}
}
