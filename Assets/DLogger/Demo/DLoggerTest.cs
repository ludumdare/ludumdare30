

using UnityEngine;
using System.Collections;

public class DLoggerTest : MonoBehaviour {
	
	private int i;
	
	// Use this for initialization
	void Start () {
		
		D.Error("Error Test");
		D.Error("Error Test {0}", 1);
		D.Warn("Warning Test");
		D.Warn("Warning Test {0}", 1);
		D.Log("Log Test");
		D.Log("Log Test {0}", 1);
		D.Log(LogCategory.Game, "Log Category Test {0}", 2);
		D.Detail("Detail Test");
		D.Detail("Detail Test {0}", 1);
		D.Detail(LogCategory.Game, "Log Category Detail Test {0}", 2);
		D.Trace("Trace Test");
		D.Trace("Trace Test {0}", 1);
		D.Trace(LogCategory.Game, "Log Category Trace Test {0}", 2);
	}
	
	void Update() {
		i++;	
		D.Log("Another Log Entry");
		D.Log(LogCategory.Game, "SmokeTest i = {0}", i);
		D.Log(LogCategory.Camera, "Some GameObject = " + gameObject);
	}

}
