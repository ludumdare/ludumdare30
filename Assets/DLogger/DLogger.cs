
//	Copyright 2013 SpockerDotNet LLC

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DLogger : MonoSingleton<DLogger> {
	
	public bool			m_showWatches = true;
	
	private string 		m_loggerPath = Application.persistentDataPath + "\\";
	private string 		m_loggerName = "dlogger";
	private bool		m_loggerWatch = false;
	
	private Dictionary<string, object> 	m_watchGroups;
	
	private bool		m_quit;
	
	public string LoggerPath {
		get { return this.m_loggerPath; }
		set { 
			this.m_loggerPath = value; 
			Debug.Log(string.Format("- DLogger path = {0}", m_loggerPath));
		}
	}
	public string LoggerName {
		get { return this.m_loggerName; }
		set { 
			this.m_loggerName = value; 
			Debug.Log(string.Format("- DLogger name = {0}", m_loggerName));
		}
	}
	void OnEnable() {
		Debug.Log("- DLogger started");
		m_watchGroups = new Dictionary<string, object>();
		_loadSettings();
		__initWatch();
	}
	void OnApplicationQuit() {
		Debug.LogWarning("- DLogger quitting");
		m_quit = true;
	}
	void OnDestroy() {
		if (!m_quit) return;
		D.Quit();
	}
	private void _loadSettings() {
		string json = SDNUtils.LoadJsonResourceFile("dlogger");
		Dictionary<string, object> _data = SDNUtils.JsonDeserialize(json);	
		Dictionary<string, object> _settings = _data["settings"] as Dictionary<string, object>;
		_loadLoggerSettings(_settings);
	}	
	private void _loadLoggerSettings(Dictionary<string, object> settings) {
		if (settings.ContainsKey("logPath")) m_loggerPath = (string)settings["logPath"];
		if (settings.ContainsKey("logName"))m_loggerName = (string)settings["logName"];
		if (settings.ContainsKey("logWatch"))m_loggerWatch = (bool)settings["logWatch"];
		m_showWatches = m_loggerWatch;	//	set default to what is in file
	}
	
	public void ClearWatch(string name) {
		if (m_watchGroups.ContainsKey(name)) {
			Dictionary<string, object> w = m_watchGroups[name] as Dictionary<string, object>;
			w.Clear();
			m_watchGroups[name] = w;
		}
	}
	
	public void SetWatch(string key, object val) {
		__setWatch("default", key, val);
		/*
		if (m_watch.ContainsKey(key)) {
			m_watch[key] = val;
		} else {
			m_watch.Add(key, val);
		}
		*/
	}
	
	public void SetWatch(string name, string key, object val) {
		__setWatch(name, key, val);
	}
	
	public void __setWatch(string name, string key, object val) {
		
		// when the group name does not exist, create it
		if (!m_watchGroups.ContainsKey(name)) {
			Dictionary<string, object> w = new Dictionary<string, object>();
			m_watchGroups.Add(name, w);
		}
		
		Dictionary<string, object> _w = m_watchGroups[name] as Dictionary<string, object>;
		
		if (_w.ContainsKey(key)) {
			_w[key] = val;
		} else {
			_w.Add(key, val);
		}
	}
	

	void OnLevelWasLoaded(int level) {
		__initWatch();
	}
	
	private void __initWatch() {
		m_watchGroups.Clear();	
		Dictionary<string, object> _w = new Dictionary<string, object>();
		m_watchGroups.Add("default", _w);
		
		/*
		SetWatch("string1", "string1");
		SetWatch("int", 1);
		SetWatch("float", 1f);
		SetWatch("group", "string", "string2");
		SetWatch("group", "int", 2);
		SetWatch("group", "float", 2f);
		SetWatch("group1", "string", "string2");
		SetWatch("group1", "int", 2);
		SetWatch("group1", "float", 2f);
		SetWatch("group2", "string", "string2");
		SetWatch("group2", "int", 2);
		SetWatch("group2", "float", 2f);
		SetWatch("group3", "string", "string2");
		SetWatch("group3", "int", 2);
		SetWatch("group3", "float", 2f);
		SetWatch("group4", "string", "string2");
		SetWatch("group4", "int", 2);
		SetWatch("group4", "float", 2f);
		SetWatch("group5", "string", "string2");
		SetWatch("group5", "int", 2);
		SetWatch("group5", "float", 2f);
		SetWatch("group6", "string", "string2");
		SetWatch("group6", "int", 2);
		SetWatch("group6", "float", 2f);
		SetWatch("group7", "string", "string2");
		SetWatch("group7", "int", 2);
		SetWatch("group7", "float", 2f);
		SetWatch("group8", "string", "string2");
		SetWatch("group8", "int", 2);
		SetWatch("group8", "float", 2f);
		*/
	}
	
	
	void OnGUI() {
		
		//m_loggerWatch = m_showWatches;
		
		if (!m_showWatches) return;
		
		float c = 0;
	
		float sx = 64;
		float sy = 8;
		float sw = 300;
		float sh = 20;
			
		foreach(KeyValuePair<string, object> gkvp in m_watchGroups) {
			
			int v = 0;		// current var #
			
			//x += 1;
			
			Dictionary<string, object> _w = m_watchGroups[gkvp.Key] as Dictionary<string, object>;		
				
			float _h = sh + (sh * _w.Count) + 8;
			
			if ((sy + _h) > Screen.height) { 
				sy = 8;
				c += 1;
				sx = 64 + (c * 240);
			}
			
			if (_w.Count > 0) {
				GUI.Box(new Rect(sx, sy, sw, sh + (sh * _w.Count)), gkvp.Key);
					
				foreach(KeyValuePair<string, object> kvp in _w) {
					GUI.Label(new Rect(sx, sy + sh + (sh * v), sw, sh), string.Format("{0} : {1}", kvp.Key, kvp.Value));
					v += 1;
				}
			
				sy += _h;
			}
		}
	}
}
